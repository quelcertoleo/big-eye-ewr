# Big Eye EWR

Big Eye is an EWR implementation for DCS.

## Requirements
requires MOOSE (at least 2.9.3) to run

download -> https://github.com/FlightControl-Master/MOOSE

## How to use
Use F10 menu to interact with Big Eye.
In the F10 root menu we can get the following reports:
* `Contact Report`
* `Friendly Picture`

Players can also customize frequency of contact reports, unit used (imperial / international system) using the `Preferences` submenu.

### Ground Control Intercept

Players can accept GCI tasking using the `GCI Tasking` option.

When a player enters GCI tasking the picture report will stop and there will be only focused reports on a target assigned by GCI. Contact Reports can still be requested through `F10 -> Contact Report` menu.

This assigned target will be the highest closest bogey.

Once the target is destroyed, be it by the player or other event, the player can request another target using `GCI Tasking` option.

## Mission Editor Requirements
Due to DCS limitations there should be one Client unit per DCS Group.

There is a limitation in DCS that makes it impossible to have text reports sent to client, instead they can only be sent to groups.

In order to clean schedulers for clients that leave the game or change unit, there is a cleanup system that removes scheduler.
Again, because of DCS limitations, these schedulers target DCS Groups, not DCS Units.

So, if one member of a group leaves the mission, then the other member(s) of the group will stop receiving reports.

## Configuration
* `EWR.displayMessageTime` = in seconds, onscreen time of report messages before fading out
* `EWR.highReportFrequencyPreference` = in seconds, can't be less than EWRS.displayMessageTime
* `EWR.defaultReportFrequencyPreference` = in seconds
* `EWR.lowReportFrequencyPreference` = in seconds
* `EWR.speedCutoff` = speed cutoff for slow targets that will not be detected
* `EWR.altitudeAglCutoff` = altitude cutoff for low targets that will not be detected
* `EWR.isAvailableNCTR` = enable/disable NCTR (Non Cooperative Target Recognition for scenarios post 2000)

## Feature Set
* uses in-game units to detect enemy airborne units
* players can configure report frequency, report format details and opt-in/out of automated reports
* detected units are sorted by distance to client reference
* friendly picture
* GCI mode
* switch to 12-hour format for reporting when units closer than 5 miles
* zero-configuration on mission editor, no need for group naming prefixes

## Planned
* merge detector
* focus closest unit after merge and report will focus on closest unit

### Credits
* new implementation by quelcertoleo
* original EWRS script updated by Apple 30/11/2023
* original EWRS concept by Steggles - https://github.com/Bob7heBuilder/EWRS